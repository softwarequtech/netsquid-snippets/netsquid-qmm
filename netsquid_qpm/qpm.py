"""Quantum Memory Manager snippet for NetSquid.
"""
import inspect
import queue

from collections import namedtuple

from netsquid.simutil import sim_time
from netsquid.components.component import Component


ExecTime = namedtuple("ExecTime", ["start", "end"])
ExecTime.__doc__ = """\
Information about the programs execution time.

Parameters
----------
start : float
    Program start time.
end : float
    Program end time.
"""


# pylint: disable=R0903
# Reason: component exists to provide one function only.
class QuantumProgramManager(Component):
    """Module to manage program access to the quantum processor. Components
    wishing to execute a program on the quantum process must execute it through
    the QuantumProgramManager which will schedule access to the processor.
    Currently the scheduling is done on a first-come-first-served basis."""

    def __init__(self, device):
        """Constructor

        Parameters
        ----------
        device : netsquid.components.qprocessor.QuantumProcessor
            The quantum processor that will execute the programs.
        """
        self._busy = False
        self._done_cbk = None
        self._fail_cbk = None
        self._start_time = 0.0
        self._q = queue.Queue()
        self.qubits = []

        self._dev = device
        self._dev.set_program_done_callback(
            self._handle_program_done, once=False
        )
        self._dev.set_program_fail_callback(
            self._handle_program_failure, once=False
        )

    def _reset(self):
        """Reset the state of the currently executing program."""
        self._busy = False
        self._done_cbk = None
        self._fail_cbk = None
        self._start_time = 0.0

    def execute_program(self, done_cbk, fail_cbk, program, qubit_mapping):
        """Queue up a program for execution. The program will be executed
        straight away if the processor is free, otherwise it is put in the
        queue.

        Parameters
        ----------
        done_cbk : function(ExecTime)
            Callback to call if the program executes successfully.
        fail_cbk : function(ExecTime)
            Callback to call if the program execution fails.
        program : netsquid.components.qprogram.QuantumProgram
            The program to execute.
        qubit_mapping : list of ints
            The qubit mapping for the program.

        """

        if self._busy:
            self._q.put((done_cbk, fail_cbk, program, qubit_mapping))
            return

        self._busy = True
        self._done_cbk = done_cbk
        self._fail_cbk = fail_cbk
        self._start_time = sim_time()

        # Grab qubit snapshot before execution starts
        self.qubits = self._dev.peek(
            positions=list(range(self._dev.num_positions))
        )

        self._dev.execute_program(program, qubit_mapping)

    def _continue(self):
        """Execute any currently queued up programs."""
        self._reset()
        if not self._q.empty():
            self.execute_program(*self._q.get())

    def _handle_cbk(self, cbk):
        """Handle the completion of a program and issue callback.

        Parameters
        ----------
        cbk : function(ExecTime)
            The function callback for a completed program.
        """
        assert self._busy

        params = {}
        cbk_sig = inspect.signature(cbk)

        if "exec_time" in cbk_sig.parameters:
            params["exec_time"] = ExecTime(
                start=self._start_time,
                end=sim_time(),
            )

        # Void the snapshot
        self.qubits = []

        cbk(**params)

        self._continue()

    def _handle_program_failure(self):
        """Handle a program failure."""
        assert self._fail_cbk is not None
        self._handle_cbk(self._fail_cbk)

    def _handle_program_done(self):
        """Handle successful program completion."""
        assert self._done_cbk is not None
        self._handle_cbk(self._done_cbk)
