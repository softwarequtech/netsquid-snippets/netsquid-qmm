"""Unit tests for the sample module.

This file will be discovered by ``python setup.py test``.

"""
import unittest
import unittest.mock as mock

from netsquid_qpm.qpm import QuantumProgramManager


class TestQuantumProgramManager(unittest.TestCase):
    """Unit tests for Quantum Program Manager.

    """

    def setUp(self):
        # Prepare the processing device
        self.device = mock.MagicMock()

        self.device.set_program_done_callback = mock.MagicMock()
        self.device.set_program_fail_callback = mock.MagicMock()

        self.device.execute_program = mock.MagicMock()

        # Mock program object to pass around
        self.program = mock.MagicMock()

        # Some qubit mapping
        self.qubit_mapping = range(5)

        self.qpm = QuantumProgramManager(self.device)

    def tearDown(self):
        pass

    def program_done(self):
        """Call progam done callback."""
        # pylint: disable=W0212
        self.qpm._handle_program_done()

    def program_fail(self):
        """Call progam fail callback."""
        # pylint: disable=W0212
        self.qpm._handle_program_failure()

    def test_init(self):
        """Verify the initialisation of the progam manager."""

        self.device.set_program_done_callback.assert_called_once()
        self.device.set_program_fail_callback.assert_called_once()

    def test_basic_exec(self):
        """Verify basic execution behaviour."""

        # Prepare the mock callbacks
        done_cbk = mock.MagicMock()
        fail_cbk = mock.MagicMock()

        # Execute a program
        self.qpm.execute_program(
            done_cbk,
            fail_cbk,
            self.program,
            self.qubit_mapping,
        )

        # Since there was no queue, this program should have been executed
        # straight away
        self.device.execute_program.assert_called_once()

        # Device completes execution and calls back to QPM
        self.program_done()

        # Verify that this triggered our program callback
        done_cbk.assert_called_once()
        fail_cbk.assert_not_called()

        done_cbk.reset_mock()
        fail_cbk.reset_mock()

        # Execute another program
        self.qpm.execute_program(
            done_cbk,
            fail_cbk,
            self.program,
            self.qubit_mapping,
        )

        # Device fails the execution
        self.program_fail()

        # Verify that the fail callback was called
        done_cbk.assert_not_called()
        fail_cbk.assert_called_once()

    def test_program_queue(self):
        """Verify execution of multiple programs."""

        # Prepare two sets of mock callbacks
        done_cbk_1 = mock.MagicMock()
        fail_cbk_1 = mock.MagicMock()

        done_cbk_2 = mock.MagicMock()
        fail_cbk_2 = mock.MagicMock()

        # Schedule two different programs
        self.qpm.execute_program(
            done_cbk_1,
            fail_cbk_1,
            self.program,
            self.qubit_mapping,
        )

        self.qpm.execute_program(
            done_cbk_2,
            fail_cbk_2,
            self.program,
            self.qubit_mapping,
        )

        # Execute program should have only been called once
        self.device.execute_program.assert_called_once()
        self.device.execute_program.reset_mock()

        # Issue a callback for the first program
        self.program_done()

        # Verify the first callback was called
        done_cbk_1.assert_called_once()
        done_cbk_2.assert_not_called()

        done_cbk_1.reset_mock()
        done_cbk_2.reset_mock()

        # The second program should have been executed straight away
        self.device.execute_program.assert_called_once()

        # Issue a callback for the second program
        self.program_done()

        # Verify the first callback was called
        done_cbk_1.assert_not_called()
        done_cbk_2.assert_called_once()

    def test_exec_time_cbk(self):
        """Verify that an exec_time is provided on appropriate callbacks."""

        # Prepare the mock callbacks with exec_time as an argument. Mocks
        # generally accept any arguments so we need to provide an actual
        # function as a wrapper.
        done_cbk = mock.MagicMock()
        fail_cbk = mock.MagicMock()

        def done_timed_cbk(exec_time):
            self.assertGreaterEqual(exec_time.end, exec_time.start)
            done_cbk()

        def fail_timed_cbk(exec_time):
            self.assertGreaterEqual(exec_time.end, exec_time.start)
            fail_cbk()

        # Execute a program
        self.qpm.execute_program(
            done_timed_cbk,
            fail_timed_cbk,
            self.program,
            self.qubit_mapping,
        )

        # Since there was no queue, this program should have been executed
        # straight away
        self.device.execute_program.assert_called_once()

        # Device completes execution and calls back to QPM
        self.program_done()

        # Verify that this triggered our program callback
        done_cbk.assert_called_once()
        fail_cbk.assert_not_called()

        done_cbk.reset_mock()
        fail_cbk.reset_mock()

        # Execute another program
        self.qpm.execute_program(
            done_timed_cbk,
            fail_timed_cbk,
            self.program,
            self.qubit_mapping,
        )

        # Device fails the execution
        self.program_fail()

        # Verify that the fail callback was called
        done_cbk.assert_not_called()
        fail_cbk.assert_called_once()


if __name__ == "__main__":  # pragma: no cover
    unittest.main(verbosity=2)
