NetSquid-QPM (1.1.1)
========================

This repository provides a Quantum Program Manager for NetSquid. 

Description
-----------

This is a user contributed _snippet_ for the [NetSquid quantum network
simulator](https://netsquid.org).

This Snippet provides a program manager that can be used to administer and
arbitrate access to a quantum node's processor to execute local quantum
programs.

Installation
------------

See the [INSTALL file](INSTALL.md) for instruction of how to install this snippet.

Usage
-----

Create a unique instance per `Node` and add as a sub-component

```
node.add_component(
    QuantumProgramManager(qprocessor), 
    "qpm",
)
```

Contact
-------

For questions, please contact the maintainer: Wojciech Kozlowski (<W.Kozlowski@tudelft.nl>).

Contributors
------------

This snippet has been developed by:
- Wojciech Kozlowski <W.Kozlowski@tudelft.nl> (maintainer)

License
-------

The NetSquid-QPM has the following license:

> Copyright 2018 QuTech (TUDelft and TNO)
> 
>   Licensed under the Apache License, Version 2.0 (the "License");
>   you may not use this file except in compliance with the License.
>   You may obtain a copy of the License at
> 
>     http://www.apache.org/licenses/LICENSE-2.0
> 
>   Unless required by applicable law or agreed to in writing, software
>   distributed under the License is distributed on an "AS IS" BASIS,
>   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
>   See the License for the specific language governing permissions and
>   limitations under the License.
