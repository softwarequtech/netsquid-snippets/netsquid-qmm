CHANGELOG
=========

2020-01-22 (1.1.1)
--------------------
- Allow for netsquid 0.7

2019-07-02 (1.1.0)
--------------------
- Add support for execution time

2019-06-27 (1.0.0)
--------------------
- Created this snippet
